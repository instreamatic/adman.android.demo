package com.instreamatic.adman.demo.base;

import android.app.Activity;

import com.instreamatic.adman.view.IAdmanViewBundleFactory;
import com.instreamatic.adman.view.core.BaseAdmanView;


public class CustomAdmanView extends BaseAdmanView {

    private IAdmanViewBundleFactory factory;

    public CustomAdmanView(final Activity context) {
        super(context);
        factory = new CustomAdmanViewBindFactory();
    }

    @Override
    public IAdmanViewBundleFactory factory() {
        return factory;
    }

}