package com.instreamatic.adman.demo.player;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.view.IAdmanView;
import com.instreamatic.adman.view.generic.DefaultAdmanView;

import java.util.Date;

/**
 * Main Window class. Used to connect to Media-service used to play ads (Service)
 *  The window consists of:
 *    - Buttons to establish connection with service and disconnect from the service
 *    - Text field, which is used to show ad events
 **/
public class DemoAdmanPlayerActivity extends Activity implements DemoAdmanPlayerService.Listener {


    final private static String TAG = "DAP_Activity";

    /** Text Field link */
    private TextView tvMessages;

    /** Link to an object which is playing an ad */
    private IAdman adman;

    /** Ad View (admanView) */
    private DefaultAdmanView admanView;

    /** Player service */
    private DemoAdmanPlayerService service;

    /** bound – state which is used to show if there is a connection with DemoAdmanPlayerService or not */
    private boolean bound;


    /** ServiceConnection object. Used in order to establish connection with Service  */
    private ServiceConnection connection = new ServiceConnection() {
        //used to determine the time of connection and/or disconnect
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            bindModule(binder);
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            unbindModule();
        }
    };


    //-----------------------------------------------------------

    /** States of the Window
     *
     * onCreate, initial setup
     * - sets up the Window
     * - asks for mic permission
     *
     **/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_demo_adman_player);

        //Text Field link
        tvMessages = (TextView) findViewById(R.id.tvEvents);
        tvMessages.setMovementMethod(new ScrollingMovementMethod());

        //mic permission
        checkPermission();
    }

    /**
     *
     * onResume(), Window receives audio focus. Establishing connection to Service.
     *
     **/
    @Override
    protected void onResume() {
        super.onResume();
        addMessage("onResume");

        //flag BIND_AUTO_CREATE, for cases when Service isn't working properly.
        Intent intent = new Intent(getApplicationContext(), DemoAdmanPlayerService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    /**
     *
     * onStop(), close the Window. Disconnect from Service.
     *
     **/
    @Override
    protected void onStop() {
        super.onStop();
        addMessage("onStop");

        unbindModule();
    }


    //-----------------------------------------------------------
    /**
     * Service link received. Starting communicating with Service
     **/
    private void bindModule(IBinder binder) {
        addMessage("bindModule");
        //received an object, that should be sent to onBind method in DemoAdmanPlayerService
        this.service = ((DemoAdmanPlayerService.B) binder).getService();
        initAdman();
    }

    /**
     * Disconnected from Service.
     **/
    private void unbindModule() {
        addMessage("unbindModule bound " + bound);
        doneAdman();
        if (service != null) {
            unbindService(connection);
            service = null;
        }
        bound = false;
    }

    /**
     * Advertising module started
     **/
    private void initAdman(){
        addMessage("initAdman bound " + bound);
        if(bound) return;
        adman = service.getAdman();

        //creating the window in order to show the banner
        //ad view element
        if(admanView == null) {
            admanView = new DefaultAdmanView(this);
        }
        adman.bindModule(admanView);

        //listening for Service messages
        service.setListener(DemoAdmanPlayerActivity.this);

        if (adman.isPlaying()){
            addMessage("initAdman. Show banner");
            admanView.show();
        }
    }

    /**
     * stop listening to messages
     **/
    private void doneAdman(){
        if (service != null) {
            service.setListener(null);
        }

        if (admanView != null) {
            admanView.close();
            if (adman != null) {
                adman.unbindModule(admanView);
            }
            admanView = null;
        }
        if (adman != null) {
            adman = null;
        }


    }
    //-----------------------------------------------------------


    /**
     * Button clicks
     **/
    public void onClickButton(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                start();
                break;
            case R.id.btnStop:
                stop();
                break;
        }
    }

    /**
     * START sent to Service
     **/
    private void start() {
        initAdman();
        Intent serviceIntent = new Intent();
        serviceIntent.setAction(DemoAdmanPlayerService.START);
        serviceIntent.setClass(getApplicationContext(), DemoAdmanPlayerService.class);
        startService(serviceIntent);
    }

    /**
     * STOP sent to Service
     **/
    private void stop() {
        doneAdman();
        Intent serviceIntent = new Intent();
        serviceIntent.setAction(DemoAdmanPlayerService.STOP);
        serviceIntent.setClass(getApplicationContext(), DemoAdmanPlayerService.class);
        startService(serviceIntent);
    }

    /**
     * RESUME sent to Service
     **/
    private void resume() {
        Intent serviceIntent = new Intent();
        serviceIntent.setAction(DemoAdmanPlayerService.RESUME);
        serviceIntent.setClass(getApplicationContext(), DemoAdmanPlayerService.class);
        startService(serviceIntent);
    }

    //-----------------------------------------------------------
    /**
     * Message logging enabled
     **/
    private void addMessage(String msg, boolean toDisplay) {
        Log.d(TAG, msg);
        final String str = msg;
        if(!toDisplay) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Date now = new Date();
                String date = String.format("%1$tT.%1$tL", now);
                tvMessages.setText(String.format("%s %s \n", date, str)+tvMessages.getText());
            }
        });
    }

    private void addMessage(String msg) {
        addMessage(msg, false);

    }
    /**
     * Service Events. look at DemoAdmanPlayerService.Listener
     **/
    @Override
    public void onMessage(String msg) {
        addMessage(msg, true);
    }

    final private static int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    /**
     * Mic access
     **/
    private void checkPermission() {
        View mLayout = (View) findViewById(R.id.main_layout);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                try {
                    Snackbar.make(mLayout, R.string.permission_record_audio_rationale,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.ok, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ActivityCompat.requestPermissions(DemoAdmanPlayerActivity.this,
                                            new String[]{Manifest.permission.RECORD_AUDIO},
                                            PERMISSIONS_REQUEST_RECORD_AUDIO);
                                }
                            })
                            .show();
                } catch (Exception ex) {

                }
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        PERMISSIONS_REQUEST_RECORD_AUDIO);

                // PERMISSIONS_REQUEST_RECORD_AUDIO is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}