package com.instreamatic.adman.demo.player;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.instreamatic.adman.Adman;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Type;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.voice.AdmanVoice;

import java.util.Timer;
import java.util.TimerTask;

/**
*
* Service class.
* Playing a song with advertising running in a fixed periods of time.
*
*
**/
public class DemoAdmanPlayerService extends Service implements AdmanEvent.Listener, AudioManager.OnAudioFocusChangeListener {

    final private static String TAG = "DAP_Service";

    /** MediaPlayer */
    DemoMediaPlayer demoPlayer;

    //----------------------------
    /** commands to control the Service - onStartCommand*/
    final public static String START = "start";
    final public static String PAUSE = "pause";
    final public static String STOP = "stop";
    final public static String RESUME = "resume";
    private String state;

    /** adding the new method to Binder, the method name is getService, returns the DemoAdmanPlayerService. */
    public class B extends Binder {
        public DemoAdmanPlayerService getService() {
            return DemoAdmanPlayerService.this;
        }
    }
    final protected B binder = new B();

    /** message interface */
    public interface Listener {
        void onMessage(String msg);
    }
    private Listener listener;
    //----------------------------


    /** object to work with advertisements */
    private IAdman adman;
    /** ad loading states:
     * used in timer startScheduler
     * false - ad preloading required
     * true - ad preloaded
     **/
    private boolean stateAdLoaded = false;
    private int stateAudioFocus = 0;

    //###########
    /** Timer */
    Handler tHandler;
    Timer timer;
    TimerTask tTask;
    long interval = 30 * 1000;
    //###########

    private DemoNotification notification;

    public DemoAdmanPlayerService() {
    }


    //----------------------------
    /**
     *Service messages
     *
     *Called when Service created
     *
    **/
    @Override
    public void onCreate() {
        super.onCreate();
        addMessage("onCreate");
        //MediaPlayer - media content of the app
        demoPlayer = new DemoMediaPlayer(this);

        //Creating Adman-object to handle ads
        initAdman();

        //Planner which is used to run ads by intervals
        initScheduler();

        notification = new DemoNotification(getApplicationContext(), 1);
        startForeground(notification.id, notification.notification);
    }

    /**
     *
     * Called when Service stopped working
     *
     **/
    @Override
    public void onDestroy() {
        super.onDestroy();
        addMessage("onDestroy");

        stopForeground(true);
        if (notification != null) {
            notification.hide();
            notification = null;
        }

        //Return Audio Focus
        abandonAudioFocus();

        //Adman object done
        doneAdman();

        //Media Player stopped
        if(demoPlayer != null) {
            demoPlayer.done();
            demoPlayer = null;
        }
    }

    /**
     *
     * Client connects to Service
     *
     **/
    @Override
    public IBinder onBind(Intent intent) {
        addMessage("onBind");
        //using this, client can call public methods in DemoAdmanPlayerService
        return binder;
    }

    /**
     *
     * Client disconnects from Service
     *
     **/
    @Override
    public boolean onUnbind(Intent intent) {
        addMessage("onUnBind");
        setListener(null);
        return false;
    }

    /**
     *
     * Called when command received by Service
     *
     **/
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        switch (intent.getAction()) {
            case START:
                start();
                break;
            case PAUSE:
                pause(true);
                break;
            case STOP:
                stop();
                break;
            case RESUME:
                resume(true, true);
                break;
        }
        return START_STICKY;
    }

    private void changeDemoState(String state) {
        if (this.state != state) {
            this.state = state;
            notification.setDemoState(state);
        }
    }

    private void start() {
        addMessage("CMD start", true);
        changeDemoState(START);

        resume(true, true);
    }

    private void stop() {
        addMessage("CMD stop", true);
        changeDemoState(STOP);

        stopScheduler();
        stopAdman();
        demoPlayer.stop();
        abandonAudioFocus();
    }

    private void pause(boolean withAdman) {
        addMessage("CMD pause, withAdman: " + withAdman);
        changeDemoState(PAUSE);

        stopScheduler();
        demoPlayer.pause();
        if (withAdman) {
            pauseAdman();
        }
    }

    private void resume(boolean audioFocus, boolean withAdman) {
        addMessage("resume, [audioFocus, withAdman]: " +audioFocus + ", " +withAdman);
        if (audioFocus) {
            requestAudioFocus();
        }
        changeDemoState(RESUME);

        //if the last state of the player was ‘PLAYING’ but there is no actual playback, start the player
        if (adman.isPlaying()) {
            if (withAdman) resumeAdman();
        } else {
            demoPlayer.start();
            startScheduler();
        }
    }

    //----------------

    /**
    *
    * Working with an AD
    *
    * Run Ad object
    **/
    private void initAdman() {
        //Ad request params
        //requset Audio Ad
        /*
        AdmanRequest request = new AdmanRequest.Builder()
                .setSiteId(777)
                .setRegion(Region.EUROPE)
                .setType(Type.VOICE)
                .setPreview(90)
                .build();
        /**/
        //request Voice-Enabled Ad

        AdmanRequest request = new AdmanRequest.Builder()
                .setSiteId(1249)
                .setRegion(Region.GLOBAL)
                .setType(Type.VOICE)
                .build();
        /**/
        //creating Ad object
        adman = new Adman(this, request);
        //Voice-Enabled Ad module connected
        adman.bindModule(new AdmanVoice(this));
        ///subscription to events of AD, AdmanEvent.Listener
        adman.addListener(this);
    }

    /**
     *
     * Clean Ad object
     *
     **/
    private void doneAdman() {
        //Stop working with Ad object
        if (adman != null) {
            stopAdman();
            adman.reset();
            adman = null;
        }
    }

    public void preloadAdman() {
        //Ad preloading
        adman.preload();
    }


    public void startAdman() {
        //!!! Important!!! Report the Ad Placement
        adman.sendCanShow();
        //Run an Ad
        adman.play();
    }

    public void stopAdman() {
        //false - Ad should be preloaded
        stateAdLoaded = false;
        //skip the current ad
        adman.skip();
    }

    public void pauseAdman() {
        adman.pause();
    }

    public void resumeAdman() {
        adman.play();
    }

    public IAdman getAdman() {
        return adman;
    }

    /**
     *
     * listener for Ad object events, AdmanEvent.Listener
     *
     **/
    @Override
    public void onAdmanEvent(final AdmanEvent event) {
        addMessage("onAdmanEvent: " + event.getType().name());

        switch (event.getType()) {
            case READY:
                //true - ad preloaded
                stateAdLoaded = true;
                if (demoPlayer.isPlaying()) {
                    startScheduler();
                }
                break;
            case NONE:
            case FAILED:
            case COMPLETED:
                //ad playback stopped
                demoPlayer.start();
                startScheduler();
                break;
            case STARTED:
                //ad playback started
                //false - means ad should be preloaded
                demoPlayer.pause();
                stateAdLoaded = false;
                break;
        }
    }



    //###########
    /**
     *
     * Scheduler and timer for ad playback
     * actions depends on stateAdLoaded:
     * false - start ad preloading
     * true - start ad playback
     *
     **/
    private void initScheduler() {
        timer = new Timer();
        if(tHandler == null) {
            tHandler = new Handler();
        }
    }

    /**
     *
     * Need synchronized
     * startScheduler might be call from different threads
     *
     **/
    private synchronized void startScheduler() {
        stopScheduler();
        if (interval > 0) {
            //start ad playback or ad preload
            tTask = new TimerTask() {
                public void run() {
                    stopScheduler();
                    tHandler.post(new Runnable() {
                        @Override
                        public void run()    {
                            addMessage(String.format("TIME STAMP %s Ad", stateAdLoaded ?"start" :"preload"), true);
                            if (stateAdLoaded) {
                                startAdman();
                            } else {
                                preloadAdman();
                            }

                        }
                    });
                }
            };
            int delay = stateAdLoaded
                    ? Math.round((interval*4)/5)
                    : Math.round(interval/5);
            addMessage(String.format("schedule start task after %d milliSec", delay));
            timer.schedule(tTask, delay, delay);
        }
    }

    private void stopScheduler() {
        if (tTask != null){
            addMessage("schedule stop (cancel)");
            tTask.cancel();
            tTask = null;
        } else {
            addMessage("schedule stop");
        }
    }
    //###########



    //************
    /**
     *
     * Service which is used to request audio focus requestAudioFocus, look at start()
     *
     * In the moment of ad START, there is an event AUDIOFOCUS_LOSS_TRANSIENT and Service will stop audio playback
     * In the moment of ad STOP, there is an event AUDIOFOCUS_GAIN and Service will resume audio playback
     *
     **/

    @Override
    public void onAudioFocusChange(int focusChange) {
        String event = "";
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS:
                //Stop the playback
                event = "AUDIOFOCUS_LOSS";
                stop();
                notification.setDemoState(PAUSE);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                //Pause the playback and mark that audio focus was temporary lost
                //shoud be used in order of AUDIOFOCUS_GAIN it will be possible to resume audio playback.
                event = "AUDIOFOCUS_LOSS_TRANSIENT";
                pause(false);
                notification.setDemoState(PAUSE);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                //Change the volume or stop ad playback,
                //this should be checked as well AUDIOFOCUS_LOSS_TRANSIENT.
                event = "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK";
                demoPlayer.setVolume(-0.5f);
                break;
            case AudioManager.AUDIOFOCUS_GAIN:
                event = "AUDIOFOCUS_GAIN";
                if(stateAudioFocus == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK){
                    demoPlayer.setVolume(0.5f);
                }
                else{
                    resume(false, false);
                    notification.setDemoState(START);
                }
                break;
        }
        stateAudioFocus = focusChange;
        addMessage(String.format("onAudioFocusChange: event %s ( %d )", event, focusChange));
    }

    /**
     *
     * Ask for focus
     *
     **/
    private void requestAudioFocus() {
        stateAudioFocus = 0;
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            Log.d(TAG, "requestAudioFocus");
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                AudioAttributes mAudioAttributes =
                        new AudioAttributes.Builder()
                                .setUsage(AudioAttributes.USAGE_MEDIA)
                                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                .build();
                AudioFocusRequest mAudioFocusRequest =
                        new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                                .setAudioAttributes(mAudioAttributes)
                                .setAcceptsDelayedFocusGain(true)
                                .setOnAudioFocusChangeListener(this)
                                .build();
                audioManager.requestAudioFocus(mAudioFocusRequest);
            } else {
                audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
            }
        }
    }

    /**
     *
     * Free the focus
     *
     **/
    private void abandonAudioFocus() {
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            Log.d(TAG, "abandonAudioFocus");
            audioManager.abandonAudioFocus(this);
        }
    }
    //************

    /**
     *
     * Save the listener object for Service messages
     *
     **/
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    /**
     *
     * Logging Service messages
     *
     **/
    private void addMessage(String msg, boolean toDisplay) {
        Log.d(TAG, msg);
        if(!toDisplay) return;

        if(this.listener != null) {
            this.listener.onMessage(String.format("%s %s", TAG, msg));
        }
    }

    private void addMessage(String msg) {
        addMessage(msg, false);
    }

}