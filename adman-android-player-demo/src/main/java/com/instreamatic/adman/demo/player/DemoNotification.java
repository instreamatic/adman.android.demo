package com.instreamatic.adman.demo.player;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.RemoteViews;
import com.instreamatic.adman.voice.VoiceResponse;

import java.util.Timer;
import java.util.TimerTask;


public class DemoNotification {

    final public Notification notification;
    final public int id;

    private Context context;
    private boolean displayed;
    private Timer timer;
    private TimerTask responseTask;

    public DemoNotification(Context context, int id) {
        this.context = context;
        this.id = id;
        timer = new Timer();
        displayed = false;
        notification = build();
        notification.contentView.setOnClickPendingIntent(R.id.play, buildIntent(DemoAdmanPlayerService.START));
        notification.contentView.setOnClickPendingIntent(R.id.pause, buildIntent(DemoAdmanPlayerService.PAUSE));
        notification.contentView.setOnClickPendingIntent(R.id.cancel, buildIntent(DemoAdmanPlayerService.STOP));
    }

    protected PendingIntent buildIntent(String command) {
        Intent intent = new Intent();
        intent.setAction(command);
        intent.setClass(context, DemoAdmanPlayerService.class);
        return PendingIntent.getService(context, 0, intent, 0);
    }

    protected Notification build() {
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notififcation);

        Intent intent = new Intent();
        intent.setClass(context, DemoAdmanPlayerActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification.Builder builder = new Notification.Builder(context)
                .setContent(contentView)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_launcher);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "instreamatic",
                    context.getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_NONE);
            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
            builder.setChannelId(channel.getId());
        }
        return builder.build();
    }

    public void show() {
        displayed = true;
        redraw();
    }

    public void hide() {
        displayed = false;
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(id);
    }

    protected void redraw() {
        if (displayed) {
            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(id, notification);
        }
    }


    public void setDemoState(String state) {
        switch (state) {
            case DemoAdmanPlayerService.STOP:
                hide();
                break;
            case DemoAdmanPlayerService.START:
                notification.contentView.setViewVisibility(R.id.play, View.GONE);
                notification.contentView.setViewVisibility(R.id.pause, View.VISIBLE);
                show();
                break;
            case DemoAdmanPlayerService.PAUSE:
                notification.contentView.setViewVisibility(R.id.play, View.VISIBLE);
                notification.contentView.setViewVisibility(R.id.pause, View.GONE);
                show();
                break;
        }
    }
}
