package com.instreamatic.adman.demo.voice;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import com.instreamatic.adman.*;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.adman.voice.VoiceEvent;


public class DemoAdmanVoiceActivity extends AppCompatActivity implements AdmanEvent.Listener, VoiceEvent.Listener {

    final private static String TAG = "DemoAdmanVoice";

    private IAdman adman;
    private View mLayout;

    private View startView;
    private View loadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_adman_voice_activity);

        mLayout = findViewById(R.id.main_layout);
        checkPermission();

        AdmanRequest request = new AdmanRequest.Builder()
                .setSiteId(1249)
                .setRegion(Region.GLOBAL)
                .setType(Type.VOICE)
                .build();

        adman = new Adman(this, request);
        adman.bindModule(new DemoVoiceAdmanView(this));
        adman.bindModule(new AdmanVoice(this));
        adman.getDispatcher().addListener(AdmanEvent.TYPE, this);
        adman.getDispatcher().addListener(VoiceEvent.TYPE, this);

        startView = findViewById(R.id.start);
        loadingView = findViewById(R.id.loading);
        startView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adman.start();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        adman.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adman.play();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adman.getDispatcher().removeListener(AdmanEvent.TYPE, this);
        adman.getDispatcher().removeListener(VoiceEvent.TYPE, this);
    }

    @Override
    public void onAdmanEvent(final AdmanEvent event) {
        Log.d(TAG, "onAdmanEvent: " + event.getType().name());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (event.getType()) {
                    case PREPARE:
                        startView.setVisibility(View.GONE);
                        loadingView.setVisibility(View.VISIBLE);
                        break;
                    case NONE:
                    case FAILED:
                    case SKIPPED:
                    case COMPLETED:
                        startView.setVisibility(View.VISIBLE);
                        loadingView.setVisibility(View.GONE);
                        break;
                    case STARTED:
                        loadingView.setVisibility(View.GONE);
                        break;
                }
            }
        });
    }

    @Override
    public void onVoiceEvent(VoiceEvent event) {
        switch (event.getType()) {
            case UPDATE:
                Log.d(TAG, "onVoiceEvent: " + event.getType().name() + " -- " + event.getTranscript());
                break;
            case RESPONSE:
                Log.d(TAG, "onVoiceEvent: " + event.getType().name() + " -- " + event.getResponse().action);
                break;
            default:
                Log.d(TAG, "onVoiceEvent: " + event.getType().name());
                break;
        }
    }

    final private static int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Snackbar.make(mLayout, R.string.permission_record_audio_rationale,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ActivityCompat.requestPermissions(DemoAdmanVoiceActivity.this,
                                        new String[]{Manifest.permission.RECORD_AUDIO},
                                        PERMISSIONS_REQUEST_RECORD_AUDIO);
                            }
                        })
                        .show();
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        PERMISSIONS_REQUEST_RECORD_AUDIO);

                // PERMISSIONS_REQUEST_RECORD_AUDIO is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}