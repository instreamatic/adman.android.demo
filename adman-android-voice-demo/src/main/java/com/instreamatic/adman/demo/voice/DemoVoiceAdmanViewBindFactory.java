package com.instreamatic.adman.demo.voice;

import android.app.Activity;
import android.content.Context;

import com.instreamatic.adman.view.AdmanViewType;
import com.instreamatic.adman.view.IAdmanViewBundle;
import com.instreamatic.adman.view.core.AdmanViewBundle;
import com.instreamatic.adman.view.generic.DefaultAdmanViewBindFactory;

import java.util.HashMap;
import java.util.Map;


public class DemoVoiceAdmanViewBindFactory extends DefaultAdmanViewBindFactory{

    final Map<AdmanViewType, Integer> bindings =  new HashMap<AdmanViewType, Integer>() {{
            put(AdmanViewType.BANNER, R.id.adman_banner);
            put(AdmanViewType.LEFT, R.id.adman_left);
            put(AdmanViewType.CLOSE, R.id.adman_close);

            put(AdmanViewType.VOICE_MIC, R.id.adman_mic_active);
            put(AdmanViewType.VOICE_PROGRESS, R.id.adman_voice_progress);
            put(AdmanViewType.VOICE_CONTAINER, R.id.adman_response_container);
            put(AdmanViewType.VOICE_POSITIVE, R.id.adman_response_positive);
            put(AdmanViewType.VOICE_NEGATIVE, R.id.adman_response_negative);
        }};



    @Override
    protected IAdmanViewBundle buildVoice(Activity context) {
        return AdmanViewBundle.fromLayout(context, R.layout.adman_voice_demo_portrait, bindings);
    }

}